document.addEventListener('DOMContentLoaded', function () {
   
   //#region Variáveis Colunista
   const clnstWrapWidth = document.querySelector('.article-wrapper').scrollWidth;
   const clnstMaxWidthLeft = clnstWrapWidth - document.querySelector('.article-wrapper').clientWidth;
   
   const clnstPostCount = document.querySelectorAll('.article-wrapper article').length;
   console.log(clnstPostCount);
   
   const btnPrevArticle = document.getElementById("prevColunist");
   const btnNextArticle = document.getElementById("nextColunist");
   //#endregion
   
   //#region Variáveis Vídeos
   let btnPrevVideo = document.getElementById("prevVideo");
   let btnNextVideo = document.getElementById("nextVideo");
   
   let vdsCount = document.querySelectorAll('.videos-wrapper article').length;
   
   let vdsWrapHeight = document.querySelector(".videos-wrapper").scrollHeight;
   let vdsMaxHeightTop = vdsWrapHeight - document.querySelector(".videos-wrapper").clientHeight;
   //#endregion
   
   
   checkScrollButtons(btnPrevArticle, btnNextArticle, '.article-wrapper', 'scrollLeft', clnstMaxWidthLeft)
   checkScrollButtons(btnPrevArticle, btnNextArticle, '.article-wrapper', 'scrollLeft', clnstMaxWidthLeft)
   
   scrollToNext(btnPrevArticle, btnNextArticle, ".article-wrapper", clnstWrapWidth, "scrollLeft", clnstMaxWidthLeft, clnstPostCount)
   scrollToPrev(btnPrevArticle, btnNextArticle, ".article-wrapper", clnstWrapWidth, "scrollLeft", clnstPostCount)
   
   scrollToPrev(btnPrevVideo, btnNextVideo, ".videos-wrapper", vdsWrapHeight, "scrollTop", vdsCount)
   scrollToNext(btnPrevVideo, btnNextVideo, ".videos-wrapper", vdsWrapHeight, "scrollTop", vdsMaxHeightTop, vdsCount)

   scrollPostIntoView()
   
});


// as duas funções abaixo foram refatorações que fiz pra que, mudando apenas a assinatura, atenda tanto ao carousel dos vídeos, quanto dos colunistas
function scrollToNext(btnPrev, btnNext, wrap, wrapSize, scrollAxis, scrollMaxSizeAxis, childCounter) {
   btnNext.onclick = () => {
      btnPrev.removeAttribute('disabled');
      document.querySelector(wrap)[scrollAxis] += wrapSize / childCounter;
      
      // se o scroll seguinte vier a ultrapassar o limite do scrollAxis(Width para Colunistas e Height para os Vídeos), desabilita o botão e leva o scroll para o fim, evitando cortes/overflows que podem ocorrer devido aos gutters do flexbox
      if (document.querySelector(wrap)[scrollAxis] + (wrapSize / childCounter) > scrollMaxSizeAxis - 10) {
         document.querySelector(wrap)[scrollAxis] = scrollMaxSizeAxis;
         btnNext.setAttribute('disabled', 'disabled');
      }
   }
}

function scrollToPrev(btnPrev, btnNext, wrap, wrapSize, scrollAxis, childCounter) {
   btnPrev.onclick = () => {
      document.querySelector(wrap)[scrollAxis] = document.querySelector(wrap)[scrollAxis] - (wrapSize / childCounter);
      btnNext.removeAttribute('disabled');
      console.log(wrap, scrollAxis, wrapSize, childCounter);
      
      // se a sobra da posição atual do scroll pela a unidade de scroll for menor que "zero" (desconsiderando um gutter de segurança), isto é, scroll atual - (larguraTotalDoScroll / numeroDeElementosFilhos), significa que já não teria um próximo scroll pra esquerda, então desabilita o botão
      if(document.querySelector(wrap)[scrollAxis] - (wrapSize / childCounter) <= 24) {
         document.querySelector(wrap)[scrollAxis] = 0;
         btnPrev.setAttribute('disabled', 'disabled');
      }
   }
}

// se o scroll for de outro origem que não dos botões, como do efeito de correção a seguir, verifica se os botão vão se desabilitar ou não
function checkScrollButtons(btnPrev, btnNext, wrap, scrollAxis, scrollMaxSizeAxis) {
   document.querySelector(wrap).onscroll = () => {
      document.querySelector(wrap)[scrollAxis] == scrollMaxSizeAxis ? btnNext.setAttribute('disabled', 'disabled') : btnNext.removeAttribute('disabled');
      document.querySelector(wrap)[scrollAxis] < 1 ? btnPrev.setAttribute('disabled', 'disabled') : btnPrev.removeAttribute('disabled')
   }
}

// função pra trazer à view os últimos elementos do carousel de colunistas, já que, ao se expandirem com o hover, eles ficam pra lá do scroll
function scrollPostIntoView() { 
   document.querySelectorAll('.article-wrapper .article').forEach(article => {
      article.onmouseenter = () => {
         setTimeout(() => {
            article.scrollIntoView({ behavior: 'instant', block: 'nearest', inline: 'nearest' })
         }, 300)
      }
   })
}