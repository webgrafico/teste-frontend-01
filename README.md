# SEOX - Teste Frontend 01

Projeto elaborado para Seox para vaga de front-end. Elaborado com HTML, SASS e Javascript.

# Link de acesso hospedado no Gitlab Pages conforme solicitado

https://webgrafico.gitlab.io/teste-frontend-01/

# Instruções técnicas para desenvolvedores

- Clonar o repositório com o comando `git clone git@gitlab.com:webgrafico/teste-frontend-01.git`
- Entrar no diretório _teste-frontend-01_ com o comando `cd teste-frontend-01`
- Ter o node instalado na maquina: `https://nodejs.org/en/download/`
- Instalar todas as dependências com o comando `npm install`
- Rodar o projeto em modo dev com o comando `npm run dev`
- Acessar o link `http://127.0.0.1:8081/`

Comandos extras:

- Compilar o SASS gerando arquivos CSS no diretório public `npm run compile`
- Compilar em tempo de execusão os arquivos SASS `npm run watch`
